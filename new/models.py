from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=15, help_text='hashtag')

    created_at = models.DateTimeField(
        auto_now_add=True,
    )
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Keyword(models.Model):
    name = models.CharField(max_length=15, help_text='keyword')

    created_at = models.DateTimeField(
        auto_now_add=True,
    )
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Section(models.Model):
    name = models.CharField(max_length=15, help_text='section')

    created_at = models.DateTimeField(
        auto_now_add=True,
    )
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Picture(models.Model):
    description = models.CharField(max_length=50, help_text='section')
    source = models.CharField(max_length=50, null=True, blank=True)
    image = models.ImageField(upload_to='pictures')

    created_at = models.DateTimeField(
        auto_now_add=True,
    )

    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.description


class New(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    section = models.ForeignKey(Section, on_delete=models.PROTECT)
    owner = models.ForeignKey('user.User', on_delete=models.PROTECT, related_name='owner')
    tags = models.ManyToManyField(Tag)
    keywords = models.ManyToManyField(Keyword)
    created_at = models.DateTimeField(
        auto_now_add=True,
    )
    is_active = models.BooleanField(default=True)
    main_picture = models.ForeignKey(Picture, on_delete=models.PROTECT)


class Comment(models.Model):
    content = models.CharField(max_length=255)
    created_at = models.DateTimeField(
        auto_now_add=True,
    )
    user = models.ForeignKey('user.User', on_delete=models.PROTECT)
    new = models.ForeignKey(New, on_delete=models.PROTECT)

    is_active = models.BooleanField(default=True)


class Report(models.Model):
    new = models.ForeignKey(New, on_delete=models.PROTECT)
    description = models.TextField()
    created_at = models.DateTimeField(
        auto_now_add=True,
    )
    user = models.ForeignKey('user.User', on_delete=models.PROTECT)