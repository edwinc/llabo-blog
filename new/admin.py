from django.contrib import admin

from .models import New
from .models import Tag
from .models import Section
from .models import Keyword
from .models import Comment
from .models import Report


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass


@admin.register(New)
class NewAdmin(admin.ModelAdmin):
    pass


@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    pass


@admin.register(Keyword)
class KeywordAdmin(admin.ModelAdmin):
    pass


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    pass
